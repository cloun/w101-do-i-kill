import tkinter as tk
from typing import *
from tkinter import *
from PIL import ImageTk, Image
from tkinter import ttk
import timeit

class App(Frame):
    def __init__(self, master=None):
        super().__init__(master)

        self.master=master
        self.pack(fill=BOTH, expand=True)

        image= ImageTk.PhotoImage(Image.open("images/feint.png"))

        label= Label(self, image= image)
        label.image= image
        label.pack()

root= Tk()
app=App(root)
app.mainloop()