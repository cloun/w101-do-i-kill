import tkinter as tk
from typing import *
from tkinter import *
from PIL import ImageTk, Image
from tkinter import ttk
import time

class Application(tk.Frame):
    def __init__(self, master=None, background= "beige"):
        # initializes the frame and the root as the master frame
        super().__init__(master, background=background)
        self.master = master
        # makes a global variable to store the background colour of the GUI
        self.background= background
        # allows the frame to be resized
        self.pack(fill= BOTH, expand= True)
        # stores the astral buff on the damage spell
        self.buffVar= IntVar(value=0)
        # used for output of final damage value
        self.dmgText= StringVar(value="Regular: ___ | Crit ___")
        # stores the user inputted player buff
        self.playerBuff= StringVar(value="0")
        # stores the user inputted resistance of the enemy
        self.enemyResist= StringVar(value="0")
        # stores a non-standard astral buff inputted by the user
        self.otherBuff= StringVar()
        # stores the pip value of the spell being used
        # this is really only used when the spell varies in damage based on pips (judgement, tempest, etc.)
        self.pips= StringVar()


        """
        Each title refers to the buff being created
        First the frame is initialized, setting the parent to be the master frame, and background to be the background in the init
        Then the farme is added to the grid
        Then the image is retrieved using the getResizedImage function defined after all of this
        Then the variables are initialized for the checkboxes. These let the user decide if they want to have the buff or not
        """

        # FEINT FRAME
        feintFrame= Frame(self, background=self.background)
        feintFrame.grid(column=0, row=0)
        feintImage= ImageTk.PhotoImage(self.getResizedImage("feint"))
        self.feintVars= [IntVar(), IntVar()]

        # HEX FRAME
        hexFrame= Frame(self, background= self.background)
        hexFrame.grid(column=1, row=0)
        hexImage= ImageTk.PhotoImage(self.getResizedImage("hex"))
        self.hexVars= [IntVar(), IntVar()]

        # AMULET HEX
        amuletHexFrame= Frame(self, background= self.background)
        amuletHexFrame.grid(column=2, row=0)
        amuletHexImage= ImageTk.PhotoImage(self.getResizedImage("amuletHex"))
        self.amuletHexVars= [IntVar()]

        # BALANCEBLADE FRAME
        balanceFrame= Frame(self, background= self.background)
        balanceFrame.grid(column=0, row=1)
        balanceImage= ImageTk.PhotoImage(self.getResizedImage("balanceBlade"))
        self.balanceVars= [IntVar(), IntVar()]

        # PET BALANCEBLADE FRAME
        balancePetFrame= Frame(self, background= self.background)
        balancePetFrame.grid(column=1, row=1)
        balancePetImage= ImageTk.PhotoImage(self.getResizedImage("balancePet"))
        self.balancePetVars= [IntVar()]

        """
        Each title here corresponds to the damage spell being added

        The first three lines are the same as the first three in the buffs
        Then the entry box for the non-standard buff is created

        """

        # JUDGEMENT
        judgementFrame= Frame(self, background=self.background)
        judgementFrame.grid(column=0, row=2, columnspan=2)
        judgementImage= ImageTk.PhotoImage(self.getResizedImage("judgement"))
        self.otherEntry= Entry(judgementFrame, background="DarkSeaGreen1", state= DISABLED, textvariable= self.otherBuff)

        # ADDING THE FRAME WIDGETS
        # feint
        self.createBuff(feintFrame, feintImage, self.feintVars)
        # hex
        self.createBuff(hexFrame, hexImage, self.hexVars)
        # amulet hex
        self.createBuff(amuletHexFrame, amuletHexImage, self.amuletHexVars)
        # balanceblade
        self.createBuff(balanceFrame, balanceImage, self.balanceVars)
        # balanceblade pet
        self.createBuff(balancePetFrame, balancePetImage, self.balancePetVars)
        # judgement
        self.addJudgement(judgementFrame, judgementImage)

    # function to quickly get the images and resize them
    def getResizedImage(self, imageName):
        return Image.open("images/"+imageName+".png").resize(size=(100, 150))
    
    def createBuff(self, frame, image, variables):
        label= Label(frame, image= image, height=150, width=100)
        label.image=image
        label.grid(row=0, column=0, columnspan=2, padx=10, pady=10)

        for i in range(len(variables)):
            if(i==0):
                checkBox= Checkbutton(frame, onvalue=1, offvalue=0, borderwidth=2,\
                     text="(Regular)", background=self.background, variable= variables[i])
                checkBox.grid(row=1, column=0)
            else:
                checkBox2= Checkbutton(frame, onvalue=1, offvalue=0, borderwidth=2,\
                     text="(Buffed)", background= self.background, variable=variables[i])
                checkBox2.grid(row=1, column=1)
    
    def addJudgement(self, frame, image):
        #adds the picture
        label= Label(frame, image= image, height=150, width=100)
        label.image=image
        label.grid(row=0, column=0, columnspan=2, padx=10, pady=10)

        # adds the checkboxes for the various astral buffs for damage spells
        checkBoxEpic= Checkbutton(frame, onvalue=300, offvalue=0, borderwidth=2,\
                text="Epic", background= self.background, variable=self.buffVar, command= self.otherUpdate)
        checkBoxEpic.grid(row=1, column=0)

        checkBoxColl= Checkbutton(frame, onvalue=275, offvalue=0, borderwidth=2,\
                text="Collosal", background= self.background, variable=self.buffVar, command= self.otherUpdate)
        checkBoxColl.grid(row=1, column=1)

        checkBoxGarg= Checkbutton(frame, onvalue=225, offvalue=0, borderwidth=2,\
                text="Gargantuan", background= self.background, variable=self.buffVar, command= self.otherUpdate)
        checkBoxGarg.grid(row=2, column=0)
        # adding the other buff checkbox and the entry box for the buff
        checkboxOther= Checkbutton(frame, onvalue=1, offvalue=0, borderwidth=2,\
                text="Other", background= self.background, variable=self.buffVar, command= self.otherUpdate)
        checkboxOther.grid(row=2, column=1)
        otherTitle=  Label(frame, text="Other Buff:", background= self.background)
        otherTitle.grid(row=2, column=2)
        self.otherEntry.grid(row=2, column=3)

        # adding the pips box
        pipsTitle= Label(frame, text="Pips", background= self.background)
        pipsTitle.grid(row=1, column=2)
        pipsTextBox= Entry(frame, background="DarkSeaGreen1", textvariable= self.pips)
        pipsTextBox.grid(row=1, column=3)

        # adding the outputs for damage
        dmgFrame= self.createUpdateFrame(frame)
        dmgFrame.grid(row=0, column=2, columnspan=2)
    
    # crates the update frame for each damage spell
    def createUpdateFrame(self, masterFrame):
        updateFrame= Frame(masterFrame, background= self.background)

        # adding the player buff section
        playerBuffLabel= Label(updateFrame, text= "Player Buff", background=self.background)
        playerBuffLabel.grid(row=0, column=0, padx=2, pady=5)
        playerBuffBox= Entry(updateFrame, textvariable= self.playerBuff, background= "DarkSeaGreen1")
        playerBuffBox.grid(row=0, column=1, padx=2, pady=5)
        #adding the update button
        updateButton= Button(updateFrame, text="Update", command= self.updateDmg)
        updateButton.grid(row=1, column=0, columnspan=2, pady=5)
        #adding the damage text box
        dmgText= Label(updateFrame, textvariable=self.dmgText, relief= SUNKEN, borderwidth=2, height=1, width=30)
        dmgText.grid(row=2, column=0, columnspan=2, pady=5)

        return updateFrame
    
    # update functions for the astral buff checkboxes
    def otherUpdate(self):
        if (self.buffVar.get()==1):
            self.otherEntry.config(state= NORMAL)
        else:
            self.otherEntry.delete(0, END)
            self.otherEntry.config(state= DISABLED)
    
    def getDamage(self):
        pass

    def updateDmg(self):
        dmg=0
        if (self.buffVar.get()==1):
            dmg+= 100 * int(self.pips.get()) + int(self.otherBuff.get())
        else:
            dmg+= 100 * int(self.pips.get()) + self.buffVar.get()
        
        # doing traps
        dmg= self.trapBuff(dmg)
        # doing blades
        dmg= self.bladeBuff(dmg)

        self.dmgText.set("Regular: "+str(round(dmg))+" | Crit: "+str(round(dmg*2)))
    
    def bladeBuff(self, dmg):
        # doing balanceblades
        returnValue= dmg * (1.25**self.balanceVars[0].get()) * (1.35**self.balanceVars[1].get()) * (1.25**self.balancePetVars[0].get())

        return returnValue

    def trapBuff(self, dmg):
        # doing feints
        returnValue= dmg * (1.7**self.feintVars[0].get()) * (1.8**self.feintVars[1].get())
        # doing hexes
        returnValue= returnValue * (1.3**self.hexVars[0].get()) * (1.4**self.hexVars[1].get()) * (1.35**self.amuletHexVars[0].get())

        return returnValue

root = tk.Tk()
root.title("Wizard101 Do I Kill?")
app = Application(master=root)
app.mainloop()